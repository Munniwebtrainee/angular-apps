import { Component } from '@angular/core';
import { someTemplate } from './config';
import { calculateValue } from './utilities';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';


export const WINDOW = new InjectionToken('Window');
export function _window() { return window; }


export function myStrategy() { ... }
export function otherStrategy() { ... }
export function someValueFactory() {
  return calculateValue();
}

export function serverFactory() {
	return new Server();
}
export function wrapInArray<T>(value: T): T[] {
  return [value];
}
export let someTemplate: '<h1>Greetings from Angular</h1>';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [{provide: server, useFactory: serverFactory},
  { provide: MyStrategy, useFactory: myStrategy },
    { provide: OtherStrategy, useFactory: otherStrategy },
    { provide: SomeValue, useFactory: someValueFactory },
    { provide: WINDOW, useFactory: _window }]
})
export class AppComponent {
  @Input() hero: Hero;

  person?: Person;
  address?: Address;

  setData(person: Person, address: Address) {
    this.person = person;
    this.address = address;
  }
   constructor (@Inject(WINDOW) private win: Window) { }
   constructor (@Inject(DOCUMENT) private doc: Document) {  }

}
